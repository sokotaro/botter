# Setting up the StarCraft II Learning Environment
* Getting a Blizzard Account?
* StarCraft II Data
* StarCraft II API
* PySC2

## Getting a Blizzard Account?
* [Create Blizzard Account](https://us.battle.net/account/creation)
* [Setup Authenticator](https://us.battle.net/support/en/article/24520)
* [Enable API](https://dev.battle.net/)
* [Create New Client](https://develop.battle.net/access/clients/create)
* Get Access Token

    | key            | value                            |
    | -------------- | -------------------------------- |
    | Client ID      | --- |
    | Client Secret  | --- |

	```
    curl -u {client_id}:{client_secret} -d grant_type=client_credentials https://us.battle.net/oauth/token
	```
	
## StarCraft II Data
* Binary
  * [Linux 3.16.1](http://blzdistsc2-a.akamaihd.net/Linux/SC2.3.16.1.zip)

	| key      | value           |
	|----------|-----------------|
	| password | iagreetotheeula |

  * [win or mac](https://us.battle.net/account/download/?show=sc2)

* Maps
  * Map Packs
	* [Ladder 2017 Season 1](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2017Season1.zip)
	* [Ladder 2017 Season 2](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2017Season2.zip)
	* [Ladder 2017 Season 3 Updated](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2017Season3_Updated.zip)
	* [Ladder 2017 Season 4](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2017Season4.zip)
	* [Ladder 2018 Season 1](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2018Season1.zip)
	* [Ladder 2018 Season 2](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2018Season2_Updated.zip)
	* [Ladder 2018 Season 3](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2018Season3.zip)
	* [Ladder 2018 Season 4](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2018Season4.zip)
	* [Ladder 2019 Season 1](http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2019Season1.zip)
	* [Melee](http://blzdistsc2-a.akamaihd.net/MapPacks/Melee.zip)
  * Mini Games Maps
	* [mini-games-maps](https://github.com/deepmind/pysc2/releases/download/v1.0/mini_games.zip)

* Replay Packs
	* [3.16.1 - Pack 1](http://blzdistsc2-a.akamaihd.net/ReplayPacks/3.16.1-Pack_1-fix.zip)
	* [3.16.1 - Pack 2](http://blzdistsc2-a.akamaihd.net/ReplayPacks/3.16.1-Pack_2.zip)
